package com.demo.mybatis.books.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.mybatis.books.model.BookInfoRequest;
import com.demo.mybatis.books.model.BookInfoResponse;
import com.demo.mybatis.books.service.BooksService;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	public BooksService bookService;

	@GetMapping(value = "/all")
	public List<BookInfoResponse> getBooks() {

		return bookService.getBooks();
	}

	@GetMapping()
	public BookInfoResponse getBookById(@RequestParam(value = "title") String title) {
		return bookService.getBookById(title);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity addBook(@Valid @RequestBody BookInfoRequest req) {
		int returnType = bookService.addBook(req);
		if (returnType > 0)
			return ResponseEntity.ok("Added");
		else
			return (ResponseEntity) ResponseEntity.noContent();
	}

}
