package com.demo.mybatis.books.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.demo.mybatis.books.model.*;

@Mapper
public interface BooksMapper {
	
	@Select("SELECT * FROM books")
	public List<BookInfoResponse> findAll();
	
	@Select("SELECT * FROM books WHERE title = #{title}")
	public BookInfoResponse getBookById(String title);
	
	@Insert("INSERT INTO books(Author, Title, Published, Remark) "
			+ "VALUES (#{author}, #{title}, #{published},"
			+ " #{remark});\r\n" + 
			"")
	public int addBook(BookInfoRequest req);

}
