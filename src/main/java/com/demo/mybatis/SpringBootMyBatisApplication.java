package com.demo.mybatis;

import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.demo.mybatis.books.model.BookInfoResponse;


@MappedTypes(BookInfoResponse.class)
@MapperScan("com.demo.mybatis.books.mapper")
@SpringBootApplication
public class SpringBootMyBatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMyBatisApplication.class, args);
	}

}
